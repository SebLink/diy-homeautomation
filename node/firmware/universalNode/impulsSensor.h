#pragma once

#include "radio.h"

class ImpulsSensor {
  private:
    //variable for impulse counter:
    //time variable for debouncing interrupt
    volatile unsigned long last_interrupt_time = 0;
    volatile unsigned long interrupt_time = 0;

    //variable for reading serial interface for changing settings
    byte serial_data;     
    String inString ="";


  public:
    //constructor/destructor:
    ImpulsSensor();
    ~ImpulsSensor();

    //functions
    //interrupt service routine, counts number of interrupt
    void impulsCounterInterrupt();

    //calculates new meter reading form counted impulses, impulsvalue and last meter reading and sends it via radio
    void impulsSensorOutput(RFMRadio &radio);

    //function for reading serial interface for input to change settings of impulse Sensor (Serial Interface has to be activated for using this function)
    //send "impulsesPerValue:<value>" for changing the impulses per value and send "impulseMeterReadin:<value>" for changing the current meter reading
    void changeSettings();

    //variables:
    volatile float impulsMeterReading = 0; //variable for the last meter reading
    volatile float impulsesPerValue = 0; //variable for rotations per kWh/ m^3 o.ae.
    volatile float impulses = 0;  //number of impulses since last sending of meter reading
    volatile unsigned int debounceTime = 0; //current set debounce time of interrupt

};
