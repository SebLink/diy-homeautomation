#pragma once

#include "radio.h"

class MBusHandler {
  private:
  void readSerial(byte message[]);
  bool waitACKSlave();
  void extractZaehlerDaten(byte message[], RFMRadio &radio);
  void initialCommunication(byte address);
  void sendRequest(byte address);
  void wakeUp(int num, int delayMs);  //not used in current version

  public:
    MBusHandler();
    ~MBusHandler();

    //reads meter values from PolluCom E
    //parameter: radio: for sending via Radio, address: primary M-Bus address of meter
    //function: reads the meter readings for energy, volume, current flow and current Powerand sends them via the passed RFMRadio object and the in config.h defined IDs
    void readMBusSlave(RFMRadio &radio, byte address);
};
