#include "mBus.h"
#include "config.h"
#include "config_board.h"
#include "Arduino.h"
#include "utils.h"
#include "radio.h"
#include <SPI.h>

#ifndef MBus
  #define MBus 0
  #define MBUS_ENERGY 0
  #define MBUS_VOLUME 0
  #define MBUS_FLOW 0
  #define MBUS_POWER 0
#endif

MBusHandler::MBusHandler(){}

MBusHandler::~MBusHandler(){}

//function: read meter readings over M-Bus protocoll
//parameter:  radio: Radio Object for sending messages
//            address: address of meter 
void MBusHandler::readMBusSlave(RFMRadio &radio, byte address){

  byte message[72];                     //array for storeing serial data (length 72 is meter specific (PolluCom E))
  for(int i = 0; i<3; i++){             //try three times to read
    //wakeUp(132, int 100);         //wack up method for newer versions of PolluCom E
    initialCommunication(address);  //init communication
    if(waitACKSlave()){                   //check for acknowledgement
        sendRequest(address);       //send request for meter data
        readSerial(message);        //read data from meter (slave)
        extractZaehlerDaten(message, radio);  //extract meter readings from recieved messages and send them via radio
        return;
    }
  }  
}  

//function: sends M-Bus command SND_NKE to slave (meter), this initialises the communication
//paramter: address: primary M-Bus address of slave (meter)
void MBusHandler::initialCommunication(byte address){
  Serial.write(0x10);       //short frame
  Serial.write(0x40);       //SND_NKE
  Serial.write(address);    //slave address
  byte checksum = 0x40 + address;
  Serial.write(checksum);   //check sum
  Serial.write(0x16);       //stop
}

//function: sends M-Bus command REQ_U2D (requests class 2 Data) to slave (meter)
//parameter: address: primary M-Bus address of slave
void MBusHandler::sendRequest(byte address){
  Serial.write(0x10);       //short fram
  Serial.write(0x5B);       //REQ_NKE
  Serial.write(address);    //address
  byte checksum = 0x5B + address;
  Serial.write(checksum);   //check sum
  Serial.write(0x16);       //stop
}


//function: recieves meter readings from recieved message of slave and sends them via radio
//parameter:  message: recieved message from slave
//            radio: Radio objekt
//annotation: only for Meter type PolluCom E
void MBusHandler::extractZaehlerDaten(byte message[], RFMRadio &radio){
    //Energy: Bytes 21 - 24
    String energy = String(int(message[24]), HEX) + String(int(message[23]), HEX) +String(int(message[22]), HEX) + String(int(message[21]), HEX);
    float fEnergy = (float)energy.toInt()/1000; //MWh
    radio.Send(MBUS_ENERGY, fEnergy);   //send via radio
    delay(200);
    
    //volume: Bytes 27 - 30
    String volume = String(int(message[30]), HEX) + String(int(message[29]), HEX) +String(int(message[28]), HEX) + String(int(message[27]), HEX);
    float fVolume = (float)volume.toInt()/1000; //m^3
    radio.Send(MBUS_VOLUME, fVolume);
    delay(200);
        
    //current Flow: Bytes 33 - 36
    String flow = String(int(message[36]), HEX) + String(int(message[35]), HEX) +String(int(message[34]), HEX) + String(int(message[33]), HEX);
    float fFlow = (float)flow.toInt()/1000; // m^3/h
    radio.Send(MBUS_FLOW, fFlow);  
    delay(200);
     
    //current Power: Bytes 39 - 42
    String power = String(int(message[42]), HEX) + String(int(message[41]), HEX) +String(int(message[40]), HEX) + String(int(message[39]), HEX);
    float fPower = (float)power.toInt()/1000;   //kW
    radio.Send(MBUS_POWER, fPower);
    delay(200);
    
    radio.Sleep();

    //reset message
    for(int i=0; i < 72; i++){  //reset message array
      message[i] = 0x00;
    }
  }

//function: function for waiting/ checking for acknowledgement from slave after sending it a command
//parameter: non
bool MBusHandler::waitACKSlave(){  //Wait for an acknowledgement from the slave
    byte serial_data;
    unsigned long starttime = millis();
    unsigned long nowtime = millis();
    while(nowtime - starttime < 5000){  //wait for maximal 5 Seconds
      while(Serial.available()> 0){   //read Serial input
        serial_data = Serial.read();
        if(serial_data == 0xE5){      //check for single character 0xE5, indicates ACK
          return true;  
        }
      }
      nowtime = millis();
    }
    return false;   //if no ACK arrives return false
  }

//function: reads Serial input
//parameter: message: array for storeing recieved data
//usecase: used after command REQ_U2D for recieving the data

void MBusHandler::readSerial(byte message[]){
    byte serial_data;
    int index = 0;
    unsigned long starttime = millis();
    unsigned long nowtime = millis();
    while(nowtime - starttime < 5000){  //waiting for maximal 5 Seconds
      while(Serial.available() > 0){
        message[index] = Serial.read();
        index ++;
      }
      nowtime = millis();
    }
    //Serial.write(message,72);   //write 72 elements from array message //debug message for heterm
}

//function: activates serial interface of meter (not available on older versions of PolluCom E)
//parameter:  num: number of times 0x55 is send
//            delayMs: time in milliseconds the program is delayed after wacking up the meter
void MBusHandler::wakeUp(int num, int delayMs){
  Serial.end();
  delay(100);
  Serial.begin(2400, SERIAL_8N1);     //change Byte format of serial port to 8N1
  while(!Serial){
    ; //wait till serial port is ready
  }
  for(int i = 0; i<num; i++){         //write wake up sequenz
    Serial.write(0x55);
  }
  Serial.write(0x02);
  Serial.flush();                     //Waits for the transmission of outgoing serial data to complete
  Serial.end();
  unsigned long startDelay = millis();//setup for waiting
  unsigned long timeNow = millis();   
  Serial.begin(2400, SERIAL_8E1);     //change Byte format back to 8E1 needed for reading meter data
  while(!Serial){
    ; //wait till serial port is ready
  }
  while(abs(timeNow-startDelay)<delayMs){ //check that delayMs is waited befor resuming program
    timeNow = millis();
  }
}

  
