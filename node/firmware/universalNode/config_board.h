// You should not have to edit anything in here.
// The configuration in this file defines some names for
// the I/O pins used in the "universal node" board layout
#pragma once

#define LED           8

// battery monitor
const float R1 = 1000000.0; // resistance of R1 (1M)
const float R2 = 1000000.0; // resistance of R2 (1M)
const float R1R2 = R1 / (R1 + R2); // proportion of voltage divider
const double VOLTAGE = 3.3; // Arduino operating voltage
// was 3 on the breadboard prototype
const int BATTERY_PIN = 14;

//
// HC-SR04 hardware settings
//

// HC-SR04 Trigger/Echo pins
#define HC_SR04_TRIGGER 7
#define HC_SR04_ECHO 6
// HC-SR04 Power-pin and boot-time for powersaving
#define HC_SR04_POWER 5
#define HC_SR04_BOOTTIME_MS 100

//
// DHT22 hardware settings
//

// was 7 on the breadboard prototype
#define DHTPIN 4           // digital pin we're connected to

//
// Digital sensor. All digital sensors have the same pin
// as we only have one interrupt pin left
//

#define DIGITAL_SENSOR_PIN 3

#define IMPULS_SENSOR_PIN 3   //Same as Digital_Sensor
