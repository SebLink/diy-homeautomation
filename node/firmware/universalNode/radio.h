 #pragma once

#include <RFM69.h>

//struct for wireless data transmission
typedef struct {
    int       nodeID;     //node ID (1xx, 2xx, 3xx);  1xx = basement, 2xx = main floor, 3xx = outside
    int       deviceID;   //sensor ID (2, 3, 4, 5)
    unsigned long   uptime_ms;    //uptime in ms
    float     sensordata;     //sensor data?
    float     battery_volts;    //battery condition?
} Payload;

class RFMRadio {
  private:
    RFM69* radio;
    float batteryVoltage();

  public:
    RFMRadio();
    ~RFMRadio();
    void Init();
    void Send(int aDeviceID, float aValue);
    void Sleep();
};
