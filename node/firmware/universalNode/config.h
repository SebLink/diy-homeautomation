#pragma once

//
// Configuration section
// 1) Update encryption string "ENCRYPTKEY" and network id, node id and frequency
// 2) pick sensors to enable
//


//
// Sensors (device ids)
// uncomment to enable
//

// #define SENSOR_DIGITAL 4
//#define SENSOR_DISTANCE 5

//#define SENSOR_TEMP_HUM 6                   //////////////
#ifdef SENSOR_TEMP_HUM
  #define SENSOR_HUMIDITY 7
#endif

#define disable_PowerSave true

//Impuls-Counter Sensor:
#define IMPULS_SENSOR 8         /////////////////
#ifdef IMPULS_SENSOR
  #define IMPULS_SENSOR_NUM_IMPULSES 81
  #define start_Meter_Reading 345
  #define impulses_Per_Value 75  //number of impulses per kWh (Energy) or m^3 (Water/Gas)
  #define debounce_Time 1
#endif

//Easymeter D0:
//#define D0_SMARTMETER 9     /////////////////////
#ifdef D0_SMARTMETER
  #define D0_SMARTMETER_POWER 91
#endif

//M-Bus
//#define MBus 20   //if MBus is aktivated D0_SMARTMETER can't be activated
#ifdef MBus
  #define MBUS_ENERGY 21  //IDs for sending values
  #define MBUS_VOLUME 22
  #define MBUS_FLOW 23
  #define MBUS_POWER 24
  #define MBUS_ADDRESS 5  //M-Bus address of meter
#endif


//
// Analog sensors delay in seconds
// This is the delay to sleep before transmitting a new analog sensor reading
//

#define ANALOG_DELAY_SECONDS 600
//Analog_delay_seconds: with ferraris_sensor is the delay shorter due to interrupts
//stopping the sleep mode of the atmega


//
// Radio
//

#define NODEID        20    //unique for each node on same network
#define NETWORKID     22  //the same on all nodes that talk to each other
#define GATEWAYID     1
//Match frequency to the hardware version of the radio (uncomment one):
//#define FREQUENCY   RF69_433MHZ
#define FREQUENCY   RF69_868MHZ
//#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "1234567890123456" //exactly the same 16 characters/bytes on all nodes!
#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
#define ACK_TIME      30 // max # of ms to wait for an ack

//
// Arduino
//

#define SERIAL_BAUD   9600  //must be 9600 for GPS and D0 SmartMeter Sensor, use whatever if no GPS

//temperature / humidity  =====================================
#ifdef SENSOR_TEMP_HUM

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// time interval between sensor data is read and sent
#define TEMPERATURE_INTERVAL 360000

#endif

/*
 * General
 */

const bool debug = true;

#ifdef MBUS   //activating MBUS deactivates debug interface
  debug = false;
#endif

/*
 * End configuration section
 */
