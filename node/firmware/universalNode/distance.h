#pragma once

#include <HCSR04.h>

class DistanceSensor {
  private:
    HCSR04* sensor;

  public:
    DistanceSensor();
    ~DistanceSensor();

    int GetValue();
};
