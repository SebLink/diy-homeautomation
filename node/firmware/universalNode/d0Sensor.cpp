#include "d0Sensor.h"
#include "config.h"
#include "config_board.h"
#include "Arduino.h"
#include "utils.h"
#include "radio.h"
#include <SPI.h>

#ifndef D0_SMARTMETER
  #define D0_SMARTMETER 0
#endif

D0Sensor::D0Sensor(){}

D0Sensor::~D0Sensor(){}

//function: sensorD0SmartMeter
//parameter: RFMRadio Object for sending data
//does: reads Data from D0-Interface of meter, extracts data for meter reading and current power and sends them via RFMRAdio
void D0Sensor::sensorDOSmartMeter(RFMRadio &radio){
  time_1 = millis();
  time_2 = millis();
  D0_meter_Reading = -1;
  D0_current_Power = -1;
  //wait for some seconds:
  if(debug) Serial.println("start waiting for D0 serial data");
  while((time_1+8000)>time_2){  //waiting for 8 Seconds
  time_2 = millis();
  while (Serial.available() > 0) {
        //read data from serial interface
        serial_data = Serial.read();
        //convert byteformate from 7E1 to 8N1 bye masking parity bit (alternative: use Serial.begin(9600, SERIAL_7E1) when starting serial port, but debug is then not available over Arduino IDE Serial Monitor)
        serial_data = serial_data & 0b01111111;

        //check for end of line
        if (serial_data != '\n') {
            inString += (char)serial_data;
        }
        else {
        //Check for Meter Reading under code 1-0:1.8.0
        if(inString.indexOf("1-0:1.8.0") != -1){
          //Get meter reading from string and convert to float
          D0_meter_Reading = (inString.substring(14,30)).toFloat();
          if(debug){  //debug messages
            Serial.print("Digital meter reading: ");
            Serial.println(D0_meter_Reading);
          }
        }
        if(inString.indexOf("1-0:1.7.255") != -1){
          //Get current Power from string and convert to float
          D0_current_Power = (inString.substring(16,25)).toFloat();
          if(debug){  //debug messages
            Serial.print("current power output: ");
            Serial.println(D0_current_Power);
          }
        }
        inString = "";
        }
    }  
  }
    if(debug) Serial.println("end waiting for D0 serial Data");
  //Send meter reading and current Power output with radio
  if(D0_meter_Reading != -1){
    radio.Send(D0_SMARTMETER, D0_meter_Reading);
          if(debug){  //debug messages
            Serial.print("radio send: current meter Reading: ");
            Serial.println(D0_meter_Reading);
          }
    delay(100);
  }
  radio.Sleep();
}
