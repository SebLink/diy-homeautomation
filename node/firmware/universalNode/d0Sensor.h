#pragma once

#include "radio.h"




class D0Sensor {
  private:
  byte  serial_data;
  String inString;
  float   D0_meter_Reading;
  float   D0_current_Power;
  unsigned long time_1;
  unsigned long time_2;

  public:
    D0Sensor();
    ~D0Sensor();

    //reads meter values over D0-Interface, extracts Power and commulated Energy and sends it via radio
    //parameter RFMRAdio object for sending the data 
    void sensorDOSmartMeter(RFMRadio &radio);
};
