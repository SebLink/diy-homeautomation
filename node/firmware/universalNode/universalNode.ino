/*
Original Author: Eric Tsai
Updated by Ingo Haschler
License: CC-BY-SA, https://creativecommons.org/licenses/by-sa/2.0/
Date: 9-1-2014
Original File: UberSensor.ino
This sketch is for a wired Arduino w/ RFM69 wireless transceiver
Sends sensor data (gas/smoke, flame, PIR, noise, temp/humidity) back 
to gateway.  See OpenHAB configuration file.

Required libraries:
    • Adafruit Unified Sensor by Adafruit
    • DHT sensor library by Adafruit
    • RFM69_LowPowerLab by LowPowerLab
    • SPIFlash_LowPowerLab by LowPowerLab
    • Low-Power by Rocket Scream Electronics
    • HCSR04 by gamegine

Blink codes on startup: 1+2+3: everything OK, continous blinking: failed to init RFM module
*/

#include "config.h"
#include "config_board.h"

#include <LowPower.h>
#include "utils.h"
#include "radio.h"
#include <SPI.h>

const int SLEEPTIME_8S = 8;

RFMRadio radio;


#ifdef SENSOR_TEMP_HUM
#include "DHT.h"
// Initialize DHT sensor for 8mhz Arduino
DHT dht(DHTPIN, DHTTYPE, 2);
// NOTE: For working with a faster chip, like an Arduino Due or Teensy, you
// might need to increase the threshold for cycle counts considered a 1 or 0.
// You can do this by passing a 3rd parameter for this threshold.  It's a bit
// of fiddling to find the right value, but in general the faster the CPU the
// higher the value.  The default for a 16mhz AVR is a value of 6.  For an
// Arduino Due that runs at 84mhz a value of 30 works.
// Example to initialize DHT sensor for Arduino Due:
//DHT dht(DHTPIN, DHTTYPE, 30);

//
// Handles DHT sensor
//
void sensorTempHum() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    debugPrintln("Failed to read from DHT sensor!");
    return;
  }

  debugPrintf("Humidity=", h);
  debugPrintf("   Temp=", t);

  radio.Send(SENSOR_TEMP_HUM, t);
  blinkNTimes(1);
  radio.Send(SENSOR_HUMIDITY, h);
  blinkNTimes(1);
  radio.Sleep();
}
#endif //SENSOR_TEMP_HUM


#ifdef SENSOR_DISTANCE
#include "distance.h"
DistanceSensor distsensor;

void sensorDistance() {
  int value = distsensor.GetValue();
  debugPrintf("Distance=", value);
  radio.Send(SENSOR_DISTANCE, value);
  blinkNTimes(1);
  radio.Sleep();
}
#endif //SENSOR_DISTANCE

#ifdef SENSOR_DIGITAL
void digitalSensorInterrupt() {
  radio.Send(SENSOR_DIGITAL, 1111);
  blinkNTimes(1);
  radio.Sleep();
  blinkNTimes(1);
}
#endif //SENSOR_DIGITAL

#ifdef IMPULS_SENSOR
#include "impulsSensor.h"
ImpulsSensor impulsSensor;

void impulsCounterInterruptFunction(){    //easiest way to assign function of object to interrupt, doesnt work directly
  impulsSensor.impulsCounterInterrupt();
}
#endif  //IMPULS_SENSOR


#ifdef D0_SMARTMETER
#include "d0Sensor.h"
D0Sensor d0Sensor;
#endif  //D0_Smartmeter


//M-Bus Handler
#ifdef MBus
#include "mBus.h"
MBusHandler mBus;
#endif  //M-Bus


//variables for messuring sending intervall:
unsigned long lastTime;
unsigned long timeNow;
int analog_time_elapsed = 0;


void setup() {
  // Pin setup
  pinMode(LED, OUTPUT); // LED Pin
  pinMode(DIGITAL_SENSOR_PIN, INPUT_PULLUP); // Digital sensor
  pinMode(BATTERY_PIN, INPUT); // Battey voltage monitor

  // first signal: started :-)
  blinkNTimes(1);
  
  radio.Init();

  //select initialisation of serial interface:
  #ifdef MBUS
  Serial.begin(2400, SERIAL_8E1);
  #elif D0_SMARTMETER
  Serial.begin(9600);          //  setup serial
  while(!Serial){
    ; //wait till serial port is ready
  }
  #elif IMPULS_SENSOR
  Serial.begin(SERIAL_BAUD);
  #else
  if(debug){
      Serial.begin(SERIAL_BAUD); 
  }
  #endif

  //temperature / humidity sensor
#ifdef SENSOR_TEMP_HUM
  dht.begin();
  debugPrintln("DHT sensor enabled.");
  blinkNTimes(3);
  // send initial reading
  sensorTempHum();
#endif

#ifdef SENSOR_DISTANCE
  // send initial reading
  sensorDistance();
#endif

#ifdef SENSOR_DIGITAL
  // Digital sensor is interrupt driven
  attachInterrupt(digitalPinToInterrupt(DIGITAL_SENSOR_PIN), digitalSensorInterrupt, FALLING);
#endif

#ifdef IMPULS_SENSOR
  //set starting value for ferraris_sensor
  impulsSensor.impulsMeterReading = start_Meter_Reading;
  impulsSensor.impulsesPerValue = impulses_Per_Value;
  impulsSensor.debounceTime = debounce_Time;
  // Ferraris counter interrupt driven
  attachInterrupt(digitalPinToInterrupt(IMPULS_SENSOR_PIN), impulsCounterInterruptFunction, CHANGE);
#endif

lastTime = millis();  //set start time.

}


void loop() {

if(disable_PowerSave){  //disabled powersafe mode

  #ifdef IMPULS_SENSOR
    impulsSensor.changeSettings(); //checks serial monitor for setting changes
  #endif

  timeNow = millis();
  if(abs(timeNow - lastTime) >= (ANALOG_DELAY_SECONDS * 1000)){ //abs() for overflow of millis; *1000 for converting from seconds to milliseconds
      #ifdef SENSOR_TEMP_HUM
          sensorTempHum();
      #endif
      #ifdef SENSOR_DISTANCE
          sensorDistance();
      #endif
      #ifdef IMPULS_SENSOR
          impulsSensor.impulsSensorOutput(radio);   //calculate new meter reading from recieved impulses and send them via radio
      #endif
      #ifdef D0_SMARTMETER
          d0Sensor.sensorDOSmartMeter(radio);       //read D0-Meter and send the recieved values via radio
      #endif
      
      #ifdef MBUS
      mBus.readMBusSlave(radio, MBUS_ADDRESS);      //read M-Bus Slave wie Address MBUS_ADDRESS and sen recieved values via radio
      #endif    

      lastTime = timeNow;
  }
  
}
else{ //normal powersafe mode

if (analog_time_elapsed > ANALOG_DELAY_SECONDS) {
    analog_time_elapsed = 0;

    #ifdef SENSOR_TEMP_HUM
        sensorTempHum();
    #endif
    #ifdef SENSOR_DISTANCE
        sensorDistance();
    #endif
    #ifdef IMPULS_SENSOR
        impulsSensor.impulsSensorOutput(radio);   //calculate new meter reading from recieved impulses and send them via radio
    #endif
    #ifdef D0_SMARTMETER
        d0Sensor.sensorDOSmartMeter(radio);       //read D0-Meter and send the recieved values via radio
    #endif
    
    #ifdef MBUS
    mBus.readMBusSlave(radio, MBUS_ADDRESS);      //read M-Bus Slave wie Address MBUS_ADDRESS and sen recieved values via radio
    #endif    
  }
  else
    debugPrintf("Skipped turn. Elapsed: ", analog_time_elapsed);

  // Enter power down state for 8 s with ADC and BOD module disabled
  delay(200);
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  delay(100);

  // Add the 8 seconds we slept above
  analog_time_elapsed += SLEEPTIME_8S;

}
}//end loop
