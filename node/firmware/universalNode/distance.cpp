// Arduino universalNode firmware
// This module handles the communication with an HC_SR04 distance sensor
// It also handles powering up and down the sensor to save battery

#include "config.h"
#ifdef SENSOR_DISTANCE

#include "config_board.h"
#include "Arduino.h"
#include "distance.h"
#include "utils.h"



// Constructor. Sets up the sensor and initially powers it down
DistanceSensor::DistanceSensor() {
  sensor = new HCSR04(HC_SR04_TRIGGER, HC_SR04_ECHO);

  pinMode(HC_SR04_POWER, OUTPUT);
  digitalWrite(HC_SR04_POWER, HIGH); // PNP-transistor --> Power inverted!
}

// Destructor. Frees used memory.
DistanceSensor::~DistanceSensor() {
  delete sensor;
}

// This function gets one value from the distance sensor
// Parameter: none
// Return value: Distance in centimeters
int DistanceSensor::GetValue() {
  // Power on sensor
  digitalWrite(HC_SR04_POWER, LOW);
  // Wait for sensor to boot
  delay(HC_SR04_BOOTTIME_MS);
  // Get one measurement
  double value = sensor->dist();
  // Power down sensor
  digitalWrite(HC_SR04_POWER, HIGH);

  return value;
}

#endif //SENSOR_DISTANCE
