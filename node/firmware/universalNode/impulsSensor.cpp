#include "impulsSensor.h"
#include "config.h"
#include "config_board.h"
#include "Arduino.h"
#include "radio.h"
#include <SPI.h>
#include "utils.h"

#ifndef IMPULS_SENSOR
  #define IMPULS_SENSOR 0
  #define IMPULS_SENSOR_NUM_IMPULSES 0
#endif

    ImpulsSensor::ImpulsSensor(){}
    ImpulsSensor::~ImpulsSensor(){}

//Interrupt service routine: executed when sensor detects impulse
void ImpulsSensor::impulsCounterInterrupt(){
  interrupt_time = millis();
  if(abs(interrupt_time - last_interrupt_time) > debounceTime){  //debounce interrupt with debounceTime
    //increase impulse counter by one:
    impulses ++;
    //debug message for serial monitor
    if(debug){
      debugPrintln("interrupt impulse");
    }
    last_interrupt_time = interrupt_time;
  }
  
}

//function for sending meter value via radio
//parameter: RFMRadio Object
void ImpulsSensor::impulsSensorOutput(RFMRadio &radio){
  //calculate meter reading:
  impulsMeterReading = impulsMeterReading + impulses/(impulsesPerValue*2);
  //send value of meter:
  radio.Send(IMPULS_SENSOR, impulsMeterReading);
  //send number of impulses since last last sending
  delay(100);
  radio.Send(IMPULS_SENSOR_NUM_IMPULSES, impulses);
  if(debug){
      Serial.println("Impulses");
      Serial.println(impulses);
    }
  //reset impulse counter:
  impulses = 0;
  radio.Sleep();
  //debug message
  debugPrintf("radio_send", impulsMeterReading);
}

//function for changing setting of impulse counter via the serial interface
void ImpulsSensor::changeSettings(){
  while (Serial.available() > 0) {
        //read data from serial interface
        serial_data = Serial.read();
        //check for end of line
        if (serial_data != '\n') {
            inString += (char)serial_data;
        }
        else {  //new line has been send:
        if(debug){  
          Serial.println("message arrived:");
          Serial.println(inString);
        }  
        //Change impulsesPerValue by sending:  'impulsesPerValue: <Value>'
        if(inString.indexOf("impulsesPerValue:") != -1){
          this->impulsesPerValue = (inString.substring(17)).toFloat();
          if(debug){  //debug messages
            Serial.print("Current impulsesPerValue: ");
            Serial.println(this->impulsesPerValue);
          }
        }
        //Change impulsMeterReading by sending: 'impulseMeterReading: <Value>'
        if(inString.indexOf("impulseMeterReading:") != -1){
          this->impulsMeterReading = (inString.substring(20)).toFloat();
          if(debug){  //debug messages
            Serial.print("current impulseMeterReading: ");
            Serial.println(this->impulsMeterReading);
          }
        }
        inString = "";
        }
    }
}
