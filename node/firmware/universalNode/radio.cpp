// Arduino universalNode firmware
// This file handles the radio communication

#include "radio.h"
#include "config.h"
#include "config_board.h"
#include "utils.h"

// Constructor and destructor
// They create and destroy an instance to the RFM69 library
RFMRadio::RFMRadio() {
  radio = new RFM69;
}
RFMRadio::~RFMRadio() {
  delete radio;
}

// Reads and calculates current battery voltage
// Parameters: none
// Return value: Current voltage in volts
float RFMRadio::batteryVoltage() {
  // read the value at analog input
  int value = analogRead(BATTERY_PIN);
  float vout = (value * VOLTAGE) / 1024.0;
  float vin = vout / R1R2;
  if (vin < 0.09)
    vin = 0.0;//statement to quash undesired reading !
  return vin;
}

void RFMRadio::Init() {
  if (!radio->initialize(FREQUENCY, NODEID, NETWORKID)) {
    while(true)
      blinkNTimes(5);
  }
#ifdef IS_RFM69HW
  radio->setHighPower();
#endif
  radio->encrypt(ENCRYPTKEY);
  radio->setPowerLevel(31);
  radio->sleep();

  if (debug) {
    char buff[50];
    sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY == RF69_433MHZ ? 433 : FREQUENCY == RF69_868MHZ ? 868 : 915);
    Serial.println(buff);
  }
  // second signal: radio initialized
  blinkNTimes(2);
}

void RFMRadio::Send(int aDeviceID, float aValue) {
  //send data
  Payload msg;
  msg.nodeID = NODEID;
  msg.deviceID = aDeviceID;
  msg.uptime_ms = millis();
  msg.sensordata = aValue;
  msg.battery_volts = batteryVoltage();
  radio->send(GATEWAYID, (const void*)(&msg), sizeof(msg));
}

void RFMRadio::Sleep() {
  radio->sleep();
}
