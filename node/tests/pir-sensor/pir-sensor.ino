#include "config.h"

void setup() {
  // put your setup code here, to run once:
  pinMode(DIGITAL_SENSOR_PIN, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(digitalRead(DIGITAL_SENSOR_PIN));
}
