EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 16978 8922
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6500 5800 12400 5800
Wire Wire Line
	12400 5800 12400 4200
Wire Wire Line
	12400 4200 11800 4200
Wire Wire Line
	6500 5500 12100 5500
Wire Wire Line
	12100 5500 12100 3600
Wire Wire Line
	12100 3600 11800 3600
Wire Wire Line
	6500 5700 12300 5700
Wire Wire Line
	12300 5700 12300 4000
Wire Wire Line
	12300 4000 11800 4000
Wire Wire Line
	6500 5600 12200 5600
Wire Wire Line
	12200 5600 12200 3800
Wire Wire Line
	12200 3800 11800 3800
Wire Wire Line
	4300 4300 3900 4300
Wire Wire Line
	3900 4300 2700 4300
Connection ~ 3900 4300
Wire Wire Line
	7400 7100 7400 6600
Wire Wire Line
	7400 6600 7800 6600
Wire Wire Line
	7800 6600 7800 6700
Text Label 7400 7100 0    10   ~ 0
GND
Wire Wire Line
	6600 6900 6600 7100
Text Label 6600 6900 0    10   ~ 0
GND
Wire Wire Line
	8800 2900 8500 2900
Wire Wire Line
	8500 2900 8500 3100
Text Label 8800 2900 0    10   ~ 0
GND
Wire Wire Line
	11000 2500 11000 2400
Text Label 11000 2500 0    10   ~ 0
GND
Wire Wire Line
	7300 3900 7200 3900
Wire Wire Line
	7200 3900 7200 4100
Text Label 7300 3900 0    10   ~ 0
GND
Wire Wire Line
	10700 2500 10700 2400
Text Label 10700 2500 0    10   ~ 0
GND
Wire Wire Line
	10500 1300 10500 1200
Text Label 10500 1300 0    10   ~ 0
GND
Wire Wire Line
	1200 3000 1500 3000
Wire Wire Line
	1500 3000 1500 2900
Text Label 1200 3000 0    10   ~ 0
GND
Wire Wire Line
	6400 2200 6100 2200
Wire Wire Line
	6100 2200 6100 2900
Connection ~ 6100 2200
Text Label 6400 2200 0    10   ~ 0
GND
Wire Wire Line
	1900 2400 1900 2600
Wire Wire Line
	1900 2600 2400 2600
Wire Wire Line
	2400 2600 2900 2600
Wire Wire Line
	2900 2600 2900 2500
Wire Wire Line
	2400 2100 2400 2600
Wire Wire Line
	1900 5400 1900 4600
Wire Wire Line
	1900 4600 1900 4300
Wire Wire Line
	1900 4300 1900 3600
Wire Wire Line
	1900 3600 1900 2600
Wire Wire Line
	2400 4100 2400 4300
Wire Wire Line
	2400 4300 1900 4300
Wire Wire Line
	4300 4600 1900 4600
Wire Wire Line
	4300 3600 1900 3600
Wire Wire Line
	3300 2500 3300 2600
Wire Wire Line
	3300 2600 2900 2600
Connection ~ 2400 2600
Connection ~ 1900 2600
Connection ~ 1900 4300
Connection ~ 1900 4600
Connection ~ 1900 3600
Connection ~ 2900 2600
Text Label 1900 2400 0    10   ~ 0
GND
Wire Wire Line
	11900 4900 11900 4800
Wire Wire Line
	11900 4800 11800 4800
Wire Wire Line
	11800 4400 11900 4400
Wire Wire Line
	11900 4400 11900 4800
Wire Wire Line
	11900 4400 12700 4400
Wire Wire Line
	12700 4400 12700 4500
Connection ~ 11900 4800
Connection ~ 11900 4400
Text Label 11900 4900 0    10   ~ 0
GND
Wire Wire Line
	11400 2400 11300 2400
Wire Wire Line
	11300 2400 11300 2500
Text Label 11400 2400 0    10   ~ 0
GND
Wire Wire Line
	13100 2600 13100 2500
Text Label 13100 2600 0    10   ~ 0
GND
Wire Wire Line
	14400 2000 14500 2000
Text Label 14400 2000 0    10   ~ 0
GND
Wire Wire Line
	10300 3600 9300 3600
Wire Wire Line
	9300 3600 9300 4600
Wire Wire Line
	9300 4600 6500 4600
Wire Wire Line
	8100 6700 8100 6600
Wire Wire Line
	8100 6600 8800 6600
Wire Wire Line
	8800 6600 8800 7100
Wire Wire Line
	8500 6200 8800 6200
Wire Wire Line
	8800 6200 8800 6600
Connection ~ 8800 6600
Text Label 8100 6700 0    10   ~ 0
+3V3
Wire Wire Line
	7200 2700 7200 2800
Wire Wire Line
	7700 1700 7500 1700
Wire Wire Line
	7500 1700 7500 2800
Wire Wire Line
	7500 2800 7200 2800
Connection ~ 7200 2800
Text Label 7200 2700 0    10   ~ 0
+3V3
Wire Wire Line
	10300 4800 10200 4800
Text Label 10300 4800 0    10   ~ 0
+3V3
Wire Wire Line
	3800 3400 3700 3400
Wire Wire Line
	3700 3800 3700 3400
Wire Wire Line
	4300 3800 3700 3800
Wire Wire Line
	4300 4800 3700 4800
Wire Wire Line
	3700 4800 3700 3800
Wire Wire Line
	2900 2200 2900 1900
Wire Wire Line
	2900 1900 2700 1900
Wire Wire Line
	3700 3400 3700 1900
Wire Wire Line
	3700 1900 3300 1900
Wire Wire Line
	3300 1900 2900 1900
Wire Wire Line
	3300 2200 3300 1900
Wire Wire Line
	3300 1900 3300 1700
Connection ~ 3700 3800
Connection ~ 3700 3400
Connection ~ 2900 1900
Connection ~ 3300 1900
Text Label 3800 3400 0    10   ~ 0
+3V3
Wire Wire Line
	13800 1400 14800 1400
Text Label 13800 1400 0    10   ~ 0
+3V3
Wire Wire Line
	6100 1600 6100 1700
Wire Wire Line
	6100 1700 6100 1800
Wire Wire Line
	6400 1900 6400 1700
Wire Wire Line
	6400 1700 6100 1700
Wire Wire Line
	6500 3400 6700 3400
Wire Wire Line
	6700 3400 6700 1700
Wire Wire Line
	6700 1700 6400 1700
Connection ~ 6100 1700
Connection ~ 6400 1700
Wire Wire Line
	4300 4100 3900 4100
Wire Wire Line
	3900 4100 2700 4100
Connection ~ 3900 4100
Wire Wire Line
	8300 2400 8800 2400
Wire Wire Line
	6500 4700 9400 4700
Wire Wire Line
	9400 4700 9400 2000
Wire Wire Line
	9400 2000 10700 2000
Wire Wire Line
	10700 2000 11000 2000
Wire Wire Line
	11000 2000 11300 2000
Wire Wire Line
	11300 2000 11300 2200
Wire Wire Line
	11300 2200 11400 2200
Connection ~ 10700 2000
Connection ~ 11000 2000
Wire Wire Line
	8100 6200 8000 6200
Wire Wire Line
	8000 4800 6500 4800
Wire Wire Line
	8000 6700 8000 6200
Wire Wire Line
	8000 6200 8000 4800
Connection ~ 8000 6200
Wire Wire Line
	8200 2600 8200 4400
Wire Wire Line
	8200 4400 6500 4400
Wire Wire Line
	8800 2600 8200 2600
Wire Wire Line
	6500 4500 8300 4500
Wire Wire Line
	8300 4500 8300 2500
Wire Wire Line
	8300 2500 8800 2500
Wire Wire Line
	6600 6200 6600 5300
Wire Wire Line
	6600 5300 6500 5300
Wire Wire Line
	4300 3400 4200 3400
Wire Wire Line
	8000 2400 4300 2400
Wire Wire Line
	4300 2400 4300 3400
Connection ~ 4300 3400
Wire Wire Line
	10400 1900 10400 2200
Wire Wire Line
	10400 2200 10500 2200
Wire Wire Line
	10400 1500 10400 1200
Wire Wire Line
	1900 2100 1900 1900
Wire Wire Line
	2100 1900 1900 1900
Wire Wire Line
	1900 1500 1900 1900
Connection ~ 1900 1900
Text Label 1900 2100 0    10   ~ 0
+5V
Wire Wire Line
	6100 1000 6100 1200
Text Label 6100 1000 0    10   ~ 0
+5V
Wire Wire Line
	10300 1300 10300 1200
Text Label 10300 1300 0    10   ~ 0
+5V
Wire Wire Line
	1200 3100 1500 3100
Wire Wire Line
	1500 3100 1500 3200
Text Label 1200 3100 0    10   ~ 0
+5V
Wire Wire Line
	14800 1200 14300 1200
Text Label 14800 1200 0    10   ~ 0
+5V
Wire Wire Line
	12000 1300 12400 1300
Text Label 12000 1300 0    10   ~ 0
+5V
Wire Wire Line
	7300 3800 6800 3800
Wire Wire Line
	6500 5000 6800 5000
Wire Wire Line
	6800 5000 6800 3800
Wire Wire Line
	7300 3700 6900 3700
Wire Wire Line
	6900 3700 6900 5100
Wire Wire Line
	6900 5100 6500 5100
Wire Wire Line
	11800 4600 12700 4600
Wire Wire Line
	7200 3200 7200 3600
Wire Wire Line
	7200 3600 7300 3600
Wire Wire Line
	6900 3500 6900 3600
Wire Wire Line
	6900 3600 6700 3600
Wire Wire Line
	6700 3600 6700 4900
Wire Wire Line
	6700 4900 6500 4900
Wire Wire Line
	6900 3100 6900 3000
Wire Wire Line
	6900 3000 7000 3000
Wire Wire Line
	13100 2100 13100 2000
Wire Wire Line
	13100 2000 13300 2000
Wire Wire Line
	13300 1800 11900 1800
Wire Wire Line
	11900 1800 11900 3000
Wire Wire Line
	11900 3000 9100 3000
Wire Wire Line
	9100 3000 9100 5400
Wire Wire Line
	9100 5400 6500 5400
Wire Wire Line
	14000 2000 13900 2000
Wire Wire Line
	8300 1700 8500 1700
Wire Wire Line
	8500 1700 8500 2800
Wire Wire Line
	8500 2800 8800 2800
Wire Wire Line
	15400 1200 15400 1300
Wire Wire Line
	15400 1300 15400 1400
Wire Wire Line
	15400 1400 15400 1700
Wire Wire Line
	15400 1700 16000 1700
Connection ~ 15400 1300
Connection ~ 15400 1400
Wire Wire Line
	16000 1800 13900 1800
Wire Wire Line
	12600 1300 14800 1300
$Comp
L Sensornode-eagle-import:ATMEGA328P IC1
U 1 1 06F92037
P 5200 4500
F 0 "IC1" H 4500 2900 59  0000 L BNN
F 1 "ATMEGA328P" H 5000 5800 59  0000 L BNN
F 2 "Sensornode:DIL28-3" H 5200 4500 50  0001 C CNN
F 3 "" H 5200 4500 50  0001 C CNN
	1    5200 4500
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:RFM69HW U$1
U 1 1 BABD3B23
P 11100 3600
F 0 "U$1" H 11100 3600 50  0001 C CNN
F 1 "RFM69HW" H 11100 3600 50  0001 C CNN
F 2 "Sensornode:RFM" H 11100 3600 50  0001 C CNN
F 3 "" H 11100 3600 50  0001 C CNN
	1    11100 3600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:CRYSTALHC49S Q1
U 1 1 2AFD6247
P 3900 4200
F 0 "Q1" H 4000 4240 59  0000 L BNN
F 1 "8MHz" H 4000 4100 59  0000 L BNN
F 2 "Sensornode:HC49_S" H 3900 4200 50  0001 C CNN
F 3 "" H 3900 4200 50  0001 C CNN
	1    3900 4200
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:C2,5-3 C1
U 1 1 19F102CC
P 2500 4100
F 0 "C1" H 2560 4115 59  0000 L BNN
F 1 "20pF" H 2560 3915 59  0000 L BNN
F 2 "Sensornode:C2.5-3" H 2500 4100 50  0001 C CNN
F 3 "" H 2500 4100 50  0001 C CNN
	1    2500 4100
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:C2,5-3 C2
U 1 1 51743CEF
P 2600 4300
F 0 "C2" H 2660 4315 59  0000 L BNN
F 1 "20pF" H 2660 4115 59  0000 L BNN
F 2 "Sensornode:C2.5-3" H 2600 4300 50  0001 C CNN
F 3 "" H 2600 4300 50  0001 C CNN
	1    2600 4300
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R2
U 1 1 5CFB3BF2
P 6100 1400
F 0 "R2" H 5950 1454 59  0000 L BNN
F 1 "1M" H 5950 1285 59  0000 L BNN
F 2 "Sensornode:0207_10" H 6100 1400 50  0001 C CNN
F 3 "" H 6100 1400 50  0001 C CNN
	1    6100 1400
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R3
U 1 1 FB45FC39
P 6100 2000
F 0 "R3" H 5950 2054 59  0000 L BNN
F 1 "1M" H 5950 1885 59  0000 L BNN
F 2 "Sensornode:0207_10" H 6100 2000 50  0001 C CNN
F 3 "" H 6100 2000 50  0001 C CNN
	1    6100 2000
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:C5_2.5 C3
U 1 1 DAA3AADF
P 6400 2000
F 0 "C3" H 6460 2015 59  0000 L BNN
F 1 "0.1uF" H 6460 1815 59  0000 L BNN
F 2 "Sensornode:C5B2.5" H 6400 2000 50  0001 C CNN
F 3 "" H 6400 2000 50  0001 C CNN
	1    6400 2000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:C5_2.5 C4
U 1 1 F80B1965
P 1900 2200
F 0 "C4" H 1960 2215 59  0000 L BNN
F 1 "0.1uF" H 1960 2015 59  0000 L BNN
F 2 "Sensornode:C5B2.5" H 1900 2200 50  0001 C CNN
F 3 "" H 1900 2200 50  0001 C CNN
	1    1900 2200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:C5_2.5 C5
U 1 1 295533C0
P 2900 2300
F 0 "C5" H 2960 2315 59  0000 L BNN
F 1 "0.1uF" H 2960 2115 59  0000 L BNN
F 2 "Sensornode:C5B2.5" H 2900 2300 50  0001 C CNN
F 3 "" H 2900 2300 50  0001 C CNN
	1    2900 2300
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R4
U 1 1 98DC0D12
P 4000 3400
F 0 "R4" H 3850 3454 59  0000 L BNN
F 1 "10k" H 3850 3285 59  0000 L BNN
F 2 "Sensornode:0207_10" H 4000 3400 50  0001 C CNN
F 3 "" H 4000 3400 50  0001 C CNN
	1    4000 3400
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:31-XX S1
U 1 1 CEBFFD5E
P 11000 2200
F 0 "S1" V 11150 2125 59  0000 L BNN
F 1 "31-XX" V 10850 2325 59  0000 L BNN
F 2 "Sensornode:B3F-31XX" H 11000 2200 50  0001 C CNN
F 3 "" H 11000 2200 50  0001 C CNN
	1    11000 2200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:TEMP-HUM-SENSOR-DHT11 U$2
U 1 1 A8B04D1A
P 7900 7200
F 0 "U$2" H 7600 7800 59  0000 L BNN
F 1 "TEMP-HUM-SENSOR-DHT11" H 7600 7700 59  0000 L BNN
F 2 "Sensornode:TEMP-HUM-SENSOR-DHT11" H 7900 7200 50  0001 C CNN
F 3 "" H 7900 7200 50  0001 C CNN
	1    7900 7200
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND06
U 1 1 A07C0193
P 7400 7200
F 0 "#GND06" H 7400 7200 50  0001 C CNN
F 1 "GND" H 7300 7100 59  0000 L BNN
F 2 "" H 7400 7200 50  0001 C CNN
F 3 "" H 7400 7200 50  0001 C CNN
	1    7400 7200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+3V3 #6V03
U 1 1 D5F319E0
P 8800 7200
F 0 "#6V03" H 8800 7200 50  0001 C CNN
F 1 "+3.3V" V 8700 7000 59  0000 L BNN
F 2 "" H 8800 7200 50  0001 C CNN
F 3 "" H 8800 7200 50  0001 C CNN
	1    8800 7200
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R6
U 1 1 CC77A60B
P 8300 6200
F 0 "R6" H 8150 6254 59  0000 L BNN
F 1 "5k" H 8150 6085 59  0000 L BNN
F 2 "Sensornode:0207_10" H 8300 6200 50  0001 C CNN
F 3 "" H 8300 6200 50  0001 C CNN
	1    8300 6200
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:MCP1702 V-REG-1
U 1 1 937BFB0B
P 2400 1900
F 0 "V-REG-1" H 2168 2055 42  0000 L BNN
F 1 "MCP1702" H 2499 2061 42  0000 L BNN
F 2 "Sensornode:TO92" H 2400 1900 50  0001 C CNN
F 3 "" H 2400 1900 50  0001 C CNN
	1    2400 1900
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND07
U 1 1 9CDE76EF
P 6600 7200
F 0 "#GND07" H 6600 7200 50  0001 C CNN
F 1 "GND" H 6500 7100 59  0000 L BNN
F 2 "" H 6600 7200 50  0001 C CNN
F 3 "" H 6600 7200 50  0001 C CNN
	1    6600 7200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:LED3MM LED2
U 1 1 6A31474D
P 6600 6300
F 0 "LED2" V 6740 6120 59  0000 L BNN
F 1 "LED3MM" V 6825 6120 59  0000 L BNN
F 2 "Sensornode:LED3MM" H 6600 6300 50  0001 C CNN
F 3 "" H 6600 6300 50  0001 C CNN
	1    6600 6300
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R7
U 1 1 C9A5D452
P 6600 6700
F 0 "R7" H 6450 6754 59  0000 L BNN
F 1 "470" H 6450 6585 59  0000 L BNN
F 2 "Sensornode:0207_10" H 6600 6700 50  0001 C CNN
F 3 "" H 6600 6700 50  0001 C CNN
	1    6600 6700
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:C5_2.5 C6
U 1 1 2E21776E
P 8200 2400
F 0 "C6" H 8260 2415 59  0000 L BNN
F 1 "0.1uF" H 8260 2215 59  0000 L BNN
F 2 "Sensornode:C5B2.5" H 8200 2400 50  0001 C CNN
F 3 "" H 8200 2400 50  0001 C CNN
	1    8200 2400
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:CPOL-EUE2-5 C7
U 1 1 493D4CA5
P 3300 2300
F 0 "C7" H 3360 2315 59  0000 L BNN
F 1 "10uF" H 3360 2115 59  0000 L BNN
F 2 "Sensornode:E2-5" H 3300 2300 50  0001 C CNN
F 3 "" H 3300 2300 50  0001 C CNN
	1    3300 2300
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:PINHD-1X6 PROG1
U 1 1 06D0EEAE
P 8900 2700
F 0 "PROG1" H 8650 3125 59  0000 L BNN
F 1 "PINHD-1X6" H 8650 2300 59  0000 L BNN
F 2 "Sensornode:1X06" H 8900 2700 50  0001 C CNN
F 3 "" H 8900 2700 50  0001 C CNN
	1    8900 2700
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND08
U 1 1 DF24F9E8
P 8500 3200
F 0 "#GND08" H 8500 3200 50  0001 C CNN
F 1 "GND" H 8400 3100 59  0000 L BNN
F 2 "" H 8500 3200 50  0001 C CNN
F 3 "" H 8500 3200 50  0001 C CNN
	1    8500 3200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND09
U 1 1 928DF91D
P 11000 2600
F 0 "#GND09" H 11000 2600 50  0001 C CNN
F 1 "GND" H 10900 2500 59  0000 L BNN
F 2 "" H 11000 2600 50  0001 C CNN
F 3 "" H 11000 2600 50  0001 C CNN
	1    11000 2600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:MA03-1 PIR1
U 1 1 7DF21776
P 10400 900
F 0 "PIR1" H 10350 1130 59  0000 L BNN
F 1 "MA03-1" H 10350 600 59  0000 L BNN
F 2 "Sensornode:MA03-1" H 10400 900 50  0001 C CNN
F 3 "" H 10400 900 50  0001 C CNN
	1    10400 900 
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:MA04-1 US1
U 1 1 CF4A4794
P 7600 3800
F 0 "US1" H 7550 4030 59  0000 L BNN
F 1 "MA04-1" H 7550 3400 59  0000 L BNN
F 2 "Sensornode:MA04-1" H 7600 3800 50  0001 C CNN
F 3 "" H 7600 3800 50  0001 C CNN
F 4 "E" H 7550 3400 59  0001 L BNN "SPICEPREFIX"
	1    7600 3800
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND03
U 1 1 7679BDCC
P 7200 4200
F 0 "#GND03" H 7200 4200 50  0001 C CNN
F 1 "GND" H 7100 4100 59  0000 L BNN
F 2 "" H 7200 4200 50  0001 C CNN
F 3 "" H 7200 4200 50  0001 C CNN
	1    7200 4200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+3V3 #+3V02
U 1 1 37D36547
P 7200 2600
F 0 "#+3V02" H 7200 2600 50  0001 C CNN
F 1 "+3V3" V 7100 2400 59  0000 L BNN
F 2 "" H 7200 2600 50  0001 C CNN
F 3 "" H 7200 2600 50  0001 C CNN
	1    7200 2600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:BC547 T1
U 1 1 011D2A96
P 10600 2200
F 0 "T1" H 10300 2100 59  0000 L BNN
F 1 "BC547" H 10300 2000 59  0000 L BNN
F 2 "Sensornode:TO92" H 10600 2200 50  0001 C CNN
F 3 "" H 10600 2200 50  0001 C CNN
	1    10600 2200
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND05
U 1 1 EF27308A
P 10700 2600
F 0 "#GND05" H 10700 2600 50  0001 C CNN
F 1 "GND" H 10600 2500 59  0000 L BNN
F 2 "" H 10700 2600 50  0001 C CNN
F 3 "" H 10700 2600 50  0001 C CNN
	1    10700 2600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND010
U 1 1 845C69E3
P 10500 1400
F 0 "#GND010" H 10500 1400 50  0001 C CNN
F 1 "GND" H 10500 1300 59  0000 L BNN
F 2 "" H 10500 1400 50  0001 C CNN
F 3 "" H 10500 1400 50  0001 C CNN
	1    10500 1400
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R1
U 1 1 B13EB52F
P 10400 1700
F 0 "R1" H 10250 1759 59  0000 L BNN
F 1 "68k" H 10250 1570 59  0000 L BNN
F 2 "Sensornode:0207_10" H 10400 1700 50  0001 C CNN
F 3 "" H 10400 1700 50  0001 C CNN
	1    10400 1700
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:PINHD-1X2_90 BATT1
U 1 1 B96BFC32
P 1100 3100
F 0 "BATT1" H 850 3325 59  0000 L BNN
F 1 "PINHD-1X2_90" H 850 2900 59  0000 L BNN
F 2 "Sensornode:1X02_90" H 1100 3100 50  0001 C CNN
F 3 "" H 1100 3100 50  0001 C CNN
	1    1100 3100
	-1   0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+02
U 1 1 339DE9D3
P 1900 1400
F 0 "#P+02" H 1900 1400 50  0001 C CNN
F 1 "+5V" V 1800 1200 59  0000 L BNN
F 2 "" H 1900 1400 50  0001 C CNN
F 3 "" H 1900 1400 50  0001 C CNN
	1    1900 1400
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+03
U 1 1 00F05DF6
P 6100 900
F 0 "#P+03" H 6100 900 50  0001 C CNN
F 1 "+5V" V 6000 700 59  0000 L BNN
F 2 "" H 6100 900 50  0001 C CNN
F 3 "" H 6100 900 50  0001 C CNN
	1    6100 900 
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+04
U 1 1 A7FDB2D8
P 10300 1400
F 0 "#P+04" H 10300 1400 50  0001 C CNN
F 1 "+5V" V 10500 1300 59  0000 L BNN
F 2 "" H 10300 1400 50  0001 C CNN
F 3 "" H 10300 1400 50  0001 C CNN
	1    10300 1400
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+01
U 1 1 BCDF72C5
P 1500 3300
F 0 "#P+01" H 1500 3300 50  0001 C CNN
F 1 "+5V" V 1400 3100 59  0000 L BNN
F 2 "" H 1500 3300 50  0001 C CNN
F 3 "" H 1500 3300 50  0001 C CNN
	1    1500 3300
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND011
U 1 1 19DA8D48
P 1500 2800
F 0 "#GND011" H 1500 2800 50  0001 C CNN
F 1 "GND" H 1400 2700 59  0000 L BNN
F 2 "" H 1500 2800 50  0001 C CNN
F 3 "" H 1500 2800 50  0001 C CNN
	1    1500 2800
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND04
U 1 1 D896219B
P 6100 3000
F 0 "#GND04" H 6100 3000 50  0001 C CNN
F 1 "GND" H 6000 2900 59  0000 L BNN
F 2 "" H 6100 3000 50  0001 C CNN
F 3 "" H 6100 3000 50  0001 C CNN
	1    6100 3000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND01
U 1 1 DB8695AC
P 1900 5500
F 0 "#GND01" H 1900 5500 50  0001 C CNN
F 1 "GND" H 1800 5400 59  0000 L BNN
F 2 "" H 1900 5500 50  0001 C CNN
F 3 "" H 1900 5500 50  0001 C CNN
	1    1900 5500
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+3V3 #+3V01
U 1 1 406B5002
P 10100 4800
F 0 "#+3V01" H 10100 4800 50  0001 C CNN
F 1 "+3V3" V 10000 4600 59  0000 L BNN
F 2 "" H 10100 4800 50  0001 C CNN
F 3 "" H 10100 4800 50  0001 C CNN
	1    10100 4800
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:+3V3 #+3V03
U 1 1 A56F219E
P 3300 1600
F 0 "#+3V03" H 3300 1600 50  0001 C CNN
F 1 "+3V3" V 3200 1400 59  0000 L BNN
F 2 "" H 3300 1600 50  0001 C CNN
F 3 "" H 3300 1600 50  0001 C CNN
	1    3300 1600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND02
U 1 1 13A376EC
P 11900 5000
F 0 "#GND02" H 11900 5000 50  0001 C CNN
F 1 "GND" H 11800 4900 59  0000 L BNN
F 2 "" H 11900 5000 50  0001 C CNN
F 3 "" H 11900 5000 50  0001 C CNN
	1    11900 5000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:W237-102 X1
U 1 1 753049C4
P 11600 2400
F 0 "X1" H 11600 2435 59  0000 R TNN
F 1 "W237-102" H 11500 2255 59  0001 L BNN
F 2 "Sensornode:W237-102" H 11600 2400 50  0001 C CNN
F 3 "" H 11600 2400 50  0001 C CNN
	1    11600 2400
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:W237-102 X1
U 2 1 753049C8
P 11600 2200
F 0 "X1" H 11600 2235 59  0000 R TNN
F 1 "W237-102" H 11500 2055 59  0000 L BNN
F 2 "Sensornode:W237-102" H 11600 2200 50  0001 C CNN
F 3 "" H 11600 2200 50  0001 C CNN
	2    11600 2200
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND012
U 1 1 A5F18D96
P 11300 2600
F 0 "#GND012" H 11300 2600 50  0001 C CNN
F 1 "GND" H 11200 2500 59  0000 L BNN
F 2 "" H 11300 2600 50  0001 C CNN
F 3 "" H 11300 2600 50  0001 C CNN
	1    11300 2600
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:1053400-1 J1
U 1 1 2CE6A523
P 12800 4500
F 0 "J1" H 12650 4661 59  0000 L BNN
F 1 "1053400-1" H 12640 4330 59  0000 L BNN
F 2 "Sensornode:TE_1053400-1" H 12800 4500 50  0001 C CNN
F 3 "" H 12800 4500 50  0001 C CNN
	1    12800 4500
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:BC556 Q2
U 1 1 DA191B3C
P 7100 3000
F 0 "Q2" H 6700 3300 59  0000 L BNN
F 1 "BC556" H 6700 3200 59  0000 L BNN
F 2 "Sensornode:TO92-EBC" H 7100 3000 50  0001 C CNN
F 3 "" H 7100 3000 50  0001 C CNN
	1    7100 3000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R5
U 1 1 82AE158E
P 6900 3300
F 0 "R5" H 6750 3359 59  0000 L BNN
F 1 "3.3k" H 6750 3170 59  0000 L BNN
F 2 "Sensornode:0207_10" H 6900 3300 50  0001 C CNN
F 3 "" H 6900 3300 50  0001 C CNN
	1    6900 3300
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:CNY74-2H IC2
U 1 1 541775CA
P 13600 1900
F 0 "IC2" H 13399 2100 59  0000 L BNN
F 1 "CNY74-2H" H 13399 1599 59  0000 L BNN
F 2 "Sensornode:DIP762W51P254L991H470Q8" H 13600 1900 50  0001 C CNN
F 3 "" H 13600 1900 50  0001 C CNN
F 4 "E" H 13400 1600 59  0001 L BNN "SPICEPREFIX"
	1    13600 1900
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:CNY74-2H IC2
U 2 1 541775C6
P 14700 3000
F 0 "IC2" H 14499 3200 59  0000 L BNN
F 1 "CNY74-2H" H 14499 2699 59  0000 L BNN
F 2 "Sensornode:DIP762W51P254L991H470Q8" H 14700 3000 50  0001 C CNN
F 3 "" H 14700 3000 50  0001 C CNN
F 4 "E" H 14500 2700 59  0001 L BNN "SPICEPREFIX"
	2    14700 3000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R8
U 1 1 D81AD4FA
P 13100 2300
F 0 "R8" H 12950 2359 59  0000 L BNN
F 1 "2k2" H 12950 2170 59  0000 L BNN
F 2 "Sensornode:0207_10" H 13100 2300 50  0001 C CNN
F 3 "" H 13100 2300 50  0001 C CNN
	1    13100 2300
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND013
U 1 1 9E3D6951
P 13100 2700
F 0 "#GND013" H 13100 2700 50  0001 C CNN
F 1 "GND" H 13000 2600 59  0000 L BNN
F 2 "" H 13100 2700 50  0001 C CNN
F 3 "" H 13100 2700 50  0001 C CNN
	1    13100 2700
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:R0207_10 R9
U 1 1 49348D1B
P 14200 2000
F 0 "R9" H 14050 2059 59  0000 L BNN
F 1 "~10" H 14050 1870 59  0000 L BNN
F 2 "Sensornode:0207_10" H 14200 2000 50  0001 C CNN
F 3 "" H 14200 2000 50  0001 C CNN
	1    14200 2000
	-1   0    0    1   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND014
U 1 1 109F320A
P 14500 2100
F 0 "#GND014" H 14500 2100 50  0001 C CNN
F 1 "GND" H 14400 2000 59  0000 L BNN
F 2 "" H 14500 2100 50  0001 C CNN
F 3 "" H 14500 2100 50  0001 C CNN
	1    14500 2100
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+05
U 1 1 33ACA7EC
P 14300 1100
F 0 "#P+05" H 14300 1100 50  0001 C CNN
F 1 "+5V" V 14500 1000 59  0000 L BNN
F 2 "" H 14300 1100 50  0001 C CNN
F 3 "" H 14300 1100 50  0001 C CNN
	1    14300 1100
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:JP1Q 3.3VJUMP1
U 1 1 1E4A4F7E
P 8000 1700
F 0 "3.3VJUMP1" V 7900 1500 59  0000 L BNN
F 1 "JP1Q" V 8175 1500 59  0000 L BNN
F 2 "Sensornode:JP1" H 8000 1700 50  0001 C CNN
F 3 "" H 8000 1700 50  0001 C CNN
	1    8000 1700
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:PINHD-1X2_90 DEKO1
U 1 1 9560F755
P 15900 1800
F 0 "DEKO1" H 15650 2025 59  0000 L BNN
F 1 "PINHD-1X2_90" H 15650 1600 59  0000 L BNN
F 2 "Sensornode:1X02_90" H 15900 1800 50  0001 C CNN
F 3 "" H 15900 1800 50  0001 C CNN
	1    15900 1800
	-1   0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:JP3Q JP1
U 1 1 2F430EF2
P 15100 1300
F 0 "JP1" V 14900 1100 59  0000 L BNN
F 1 "JP3Q" V 15375 1100 59  0000 L BNN
F 2 "Sensornode:JP3Q" H 15100 1300 50  0001 C CNN
F 3 "" H 15100 1300 50  0001 C CNN
	1    15100 1300
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:+3V3 #+3V04
U 1 1 E81106EE
P 13800 1300
F 0 "#+3V04" H 13800 1300 50  0001 C CNN
F 1 "+3V3" V 13700 1100 59  0000 L BNN
F 2 "" H 13800 1300 50  0001 C CNN
F 3 "" H 13800 1300 50  0001 C CNN
	1    13800 1300
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:1N4148DO35-7 D1
U 1 1 8D141E84
P 12500 1300
F 0 "D1" H 12600 1319 59  0000 L BNN
F 1 "1N4148DO35-7" H 12600 1209 59  0000 L BNN
F 2 "Sensornode:DO35-7" H 12500 1300 50  0001 C CNN
F 3 "" H 12500 1300 50  0001 C CNN
	1    12500 1300
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+06
U 1 1 B0F39303
P 12000 1200
F 0 "#P+06" H 12000 1200 50  0001 C CNN
F 1 "+5V" V 12200 1100 59  0000 L BNN
F 2 "" H 12000 1200 50  0001 C CNN
F 3 "" H 12000 1200 50  0001 C CNN
	1    12000 1200
	1    0    0    -1  
$EndComp
Text Notes 2200 1700 0    59   ~ 0
MCP1702
$EndSCHEMATC
